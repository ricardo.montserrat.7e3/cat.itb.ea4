import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class ObjectOutputNoHeaderStream extends ObjectOutputStream
{

    public ObjectOutputNoHeaderStream(OutputStream out) throws IOException
    {
        super(out);
    }

    protected ObjectOutputNoHeaderStream() throws IOException, SecurityException
    {
        super();
    }

    @Override
    protected void writeStreamHeader() throws IOException
    {

    }
}
