import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main
{
    private void pause()
    {
        System.out.print("\n\nPress enter to continue...");
        new Scanner(System.in).nextLine();
    }

    private double inputPositiveDouble(String message)
    {
        Scanner reader = new Scanner(System.in);
        boolean isNumber;
        double userNum = -1;
        do
        {
            try
            {
                System.out.print(message);
                userNum = reader.nextDouble();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || userNum < 0);
        return userNum;
    }

    private int inputPositiveInt(String message)
    {
        Scanner reader = new Scanner(System.in);
        boolean isNumber;
        int userNum = -1;
        do
        {
            try
            {
                System.out.print(message);
                userNum = reader.nextInt();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || userNum < 0);
        return userNum;
    }

    private String inputAny(String message)
    {
        System.out.print(message);
        return new Scanner(System.in).nextLine();
    }

    private int getObjectsNum(boolean searchEmpleat)
    {
        int objects = 0;
        try
        {
            File file;
            if (searchEmpleat)
                file = new File("FitxerEmpleats.dat");
            else
                file = new File("FitxerDepartaments.dat");

            ObjectInputStream dataIN = new ObjectInputStream(new FileInputStream(file));
            while (dataIN.readObject() != null)
            {
                objects++;
            }
        }
        catch (EOFException ex) { System.out.println(); }
        catch (FileNotFoundException fnf) {System.out.println("Please, create the file first! " + fnf.getMessage());}
        catch (Exception e) {System.out.println(e.toString());}
        return objects;
    }

    private void ex2(boolean createEmpleat)
    {
        try
        {
            List elementsList;
            File file;
            if (createEmpleat)
            {
                elementsList = Arrays.asList(
                        new Classes.Empleat(1, "Montserrat", 21, 7777),
                        new Classes.Empleat(2, "Ramirez", 21, 2500),
                        new Classes.Empleat(3, "Gomez", 15, 4000),
                        new Classes.Empleat(4, "Maria", 20, 700),
                        new Classes.Empleat(5, "Popopo", 10, 7550));
                file = new File("FitxerEmpleats.dat");
            }
            else
            {
                elementsList = Arrays.asList(
                        new Classes.Departament(1, "Mojang", "Sweeden, Stockholm"),
                        new Classes.Departament(2, "Videogames", "Japan, Tokyo"),
                        new Classes.Departament(3, "Music", "Germany, Berlin"),
                        new Classes.Departament(4, "Visual Design", "China, Hong Kong"),
                        new Classes.Departament(5, "Physical Training", "Taiwan"));
                file = new File("FitxerDepartaments.dat");
            }
            ObjectOutputStream dataOS = new ObjectOutputStream(new FileOutputStream(file));
            if (!file.exists())
                if (file.createNewFile()) System.out.printf("File %s was successfully created!", file.getName());

            for (int i = 0; i < 5; i++)
            {
                dataOS.writeObject(elementsList.get(i));
            }
            dataOS.close();
        }
        catch (FileNotFoundException fnf) {System.out.println("Please, create the file first! " + fnf.getMessage());}
        catch (Exception e) {System.out.println(e.toString());}
    }

    private void ex3(boolean readEmpleats)
    {
        List<Object> elements = new ArrayList<>();
        try
        {
            File file;
            if (readEmpleats)
                file = new File("FitxerEmpleats.dat");
            else
                file = new File("FitxerDepartaments.dat");

            ObjectInputStream dataIN = new ObjectInputStream(new FileInputStream(file));
            Object temp;
            while ((temp = dataIN.readObject()) != null)
            {
                elements.add(temp);
            }
        }
        catch (EOFException ex)
        {
            if (readEmpleats)
                System.out.println("ID|       Cognom         | Departament | Salari");
            else
                System.out.println("ID|        Nom           |      Localitat     ");

            for (Object element : elements)
            {
                if (readEmpleats)
                {
                    Classes.Empleat empleat = (Classes.Empleat) element;
                    System.out.printf("%d | %.20s |    %d       | %.2f %n", empleat.getId(), empleat.getCognom() + "                    ", empleat.getDepartament(), empleat.getSalari());
                }
                else
                {
                    Classes.Departament departament = (Classes.Departament) element;
                    System.out.printf("%d | %.20s | %s %n", departament.getId(), departament.getNom() + "                    ", departament.getLocalitat() + "                    ");
                }
            }
        }
        catch (FileNotFoundException fnf) {System.out.println("Please, create the file first! " + fnf.getMessage());}
        catch (Exception e) {System.out.println(e.toString());}
    }

    private void ex4(boolean addEmpleat)
    {
        File file;
        try
        {
            if (addEmpleat)
                file = new File("FitxerEmpleats.dat");
            else
                file = new File("FitxerDepartaments.dat");

            if (file.exists())
            {
                ObjectOutputNoHeaderStream dataOS = new ObjectOutputNoHeaderStream(new FileOutputStream(file, true));
                int objects;
                if (addEmpleat)
                    objects = getObjectsNum(true);
                else
                    objects = getObjectsNum(false);

                for (int i = objects; i < objects + 5; i++)
                {
                    if (addEmpleat)
                        dataOS.writeObject(new Classes.Empleat(i + 1, inputAny("Lastname employee " + (i - 5) + " to add: "), inputPositiveInt("Department employee " + (i - 5) + " to add: "), inputPositiveDouble("Salary employee " + (i - 5) + " to add: ")));
                    else
                        dataOS.writeObject(new Classes.Departament(i + 1, inputAny("Department " + (i - 5) + " to add name: "), inputAny("Department " + (i - 5) + " to add location: ")));
                }
                System.out.println("Values added successfully!");
                dataOS.close();
            }
            else
            {
                System.out.println("File " + file.getName() + " doesn't exist!");
            }
        }
        catch (FileNotFoundException fnf) {System.out.println("Please, create the file first! " + fnf.getMessage());}
        catch (Exception e) {System.out.println(e.toString());}
    }

    private void optionCreate()
    {
        int decision = inputPositiveInt("Write \'1\' to create an employee file or \'2\' for an department file: ");
        if (decision == 1) ex2(true);
        else if (decision == 2) ex2(false);
        else System.out.println("\nGoing back to main menu...");
    }

    private void optionRead()
    {
        int decision = inputPositiveInt("Write \'1\' to read the employee file or \'2\' for the department file: ");
        if (decision == 1) ex3(true);
        else if (decision == 2) ex3(false);
        else System.out.println("\nGoing back to main menu...");
    }

    private void optionAdd()
    {
        int decision = inputPositiveInt("Write \'1\' to add 5 lines to the employee file or \'2\' for the department file: ");
        if (decision == 1) ex4(true);
        else if (decision == 2) ex4(false);
        else System.out.println("\nGoing back to main menu...");
    }

    private boolean getInputMenu(String userSelection)
    {
        switch (userSelection.toLowerCase())
        {
            case "2": case "create": case "c": optionCreate();
            break;
            case "3": case "read": case "r": optionRead();
            break;
            case "4": case "add": case "a": optionAdd();
            break;
            case "5": case "exit": case "e": return true;
            default: return false;
        }
        pause();
        return false;
    }

    private void showMenu()
    {
        System.out.print("---------- Welcome To My Program ----------\n\n" +
                "Ex2[create][c].- Create default files.\n" +
                "Ex3[read][r]  .- Read files.\n" +
                "Ex4[add][a]   .- Add 5 values to the files.\n" +
                "Ox5[exit][e]  .- Exit\n");
    }

    private void menu()
    {
        boolean finished;
        do
        {
            showMenu();
            finished = getInputMenu(inputAny("\nSelect an option by [command] or number: "));
        }
        while (!finished);
        System.out.println("\nGoodbye, closing program :D!");
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.menu();
    }
}
