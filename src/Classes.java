import java.io.Serializable;

public class Classes
{
    static class Empleat implements Serializable
    {
        private int id, departament;
        private String cognom;
        private double salari;

        public Empleat(int id, String cognom, int departament, double salari)
        {
            this.id = id;
            this.cognom = cognom;
            this.departament = departament;
            this.salari = salari;
        }

        public int getId()
        {
            return id;
        }

        public int getDepartament()
        {
            return departament;
        }

        public String getCognom()
        {
            return cognom;
        }

        public double getSalari()
        {
            return salari;
        }
    }

    static class Departament implements Serializable
    {
        private int id;
        private String nom, localitat;

        public Departament(int id, String nom, String localitat)
        {
            this.id = id;
            this.nom = nom;
            this.localitat = localitat;
        }

        public int getId()
        {
            return id;
        }

        public String getNom()
        {
            return nom;
        }

        public String getLocalitat()
        {
            return localitat;
        }
    }
}
